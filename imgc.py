"""
MIT License

Copyright (c) 2021 Hamjak Debbarma <hamjakdb@yahoo.com> 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os, sys
import argparse
from PIL import Image
from pathlib import Path

"""
Usage: 

1. To compressed jpg/jpeg images 
    python3 -jpg -q [10-95] -f [image file]
2. To convert from jpg/jpeg to png 
    python3 -cpng -f [image file]
3. To convert from png to jpg/jpeg  
    python3 -cjpg -f [image file]
4. Query for help 
    python3 -h
"""

#TODO: Prompt for the name of compressed file
def compress_img_jpg(file, quality):
  img = Image.open(file)
  req_quality = int(quality)
  img.save('compressed_'+file,'JPEG',optimze=True,quality=req_quality)
  return
  
# Convert the PNG image to JPEG
# imgc -cjpg -f image.png 
def to_jpg(file):
  img = Image.open(file)
  # Get rid of A(Alpha) from RGBA 
  # since jpg does not support transparency
  rbg_img = img.convert('RGB')
  filename = img.filename.split('.png')
  rbg_img.save(filename[0]+'.jpg')

# Convert JPEG image file to PNG
# imgc -cpng -f image.jpg
def to_png(file):
  img = Image.open(file)
  filename = img.filename.split('.jpg')
  img.save(filename[0]+'.png')
  
def imgc():
  parser = argparse.ArgumentParser(description='imgc: A simple image converter/compressor')
  parser.add_argument('-jpg',
                      action='store_true',
                      help='compress the jpg file format')
  parser.add_argument('-cjpg',
                     action='store_true',
                     help='convert png to jpg')
  parser.add_argument('-cpng',
                      action='store_true',
                      help='convert jpeg to png')
  parser.add_argument('-q',
                      action='store',
                      help='specify the quality for compression')
  parser.add_argument('-f', action='store', help='image file')
  parser.add_argument('-v','--version',
                      action='version',
                      version='%(prog)s 1.0')

  args = parser.parse_args()
  image_file = args.f

  if args.jpg:
    compress_img_jpg(image_file, args.q)
  elif args.cjpg:
    to_jpg(image_file)
  elif args.cpng:
    to_png(image_file)
  else:
    print("Error : invalid options")
    sys.exit()

  """
  img_size = os.path.getsize(img_file)
  img_size_in_mb = round(img_size/1000000,3)
  print(f'Image Size: {img_size_in_mb} MB')

  compress_img(img_file)
  """
# start program
imgc()
